<?php 
    include '../includes/conexion.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Probando con ajax</title>
    <link rel="stylesheet" href="../css/estilos.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>
<body style="background: #eeeeee;">
    <div class="container">
        <div class="menu">
            <div class="row">
                <!-- Inicio titulo -->
                <div class="col-md-12">    
                    <div class="titulo">
                        <h3>CRUD Marcas</h3>
                        <hr style="max-width: 60%;">
                        <h4>Agregar marca</h4>
                        <br>
                    </div>
                </div>
                <!-- Fin titulo -->

                <!-- Inicio formulario -->
                <div class="col-md-12">
                    <div class="formulario">
                        <form action="../procesos/agregar.php" method="POST">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre de la marca">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-block btn-info">Agregar</button>
                                </div>
                            </div>
                            
                        </form>
                    </div>
                </div>
                <!-- Fin formulario -->

            </div>
        </div>
    </div>
    
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <script>
        $(document).ready(function(){
            $('form').submit(function(event){
                var formData = {
                    'nombre': $('input[name=nombre]').val()
                };

                $.ajax({
                    type: 'POST',
                    url: '../procesos/agregar.php',
                    data: formData,
                    dataType: 'json',
                    encode: true
                })
                .done(function(data){
                    if(!data.success){
                        if(data.errors.nombre){
                            Swal.fire({
                                icon: 'error',
                                title: 'Ha ocurrido un error',
                                text: data.errors.nombre
                            })
                        }
                    }else{
                        Swal.fire({
                            icon: 'success',
                            title: 'Registro exitoso',
                            text: "La marca se ha registrado con éxito",

                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Continuar'
                        }).then((result) => {
                            if (result.value) {
                                location.href = "../index.php";
                            }
                        })
                    }
                })
                .fail(function(data){
                    console.log(data);
                });
                event.preventDefault();
            });
        });
    </script>
</body>
</html>