<?php

include "../includes/conexion.php";

$errors         = array(); 
$data           = array(); 

if (empty($_POST['nombre']))
    $errors['nombre'] = 'Debe ingresar el nombre';

if (!empty($errors)) {
    $data['success'] = false;
    $data['errors']  = $errors;
} else {
    $nombre = $_POST['nombre'];
    $id = $_POST['id_marca'];

    if ($nombre != '') {
        $sql_marca = "UPDATE marca SET nombre_marca='$nombre' WHERE id_marca=$id";
        $consulta_marca = $conexion->query($sql_marca);
    }

    $data['success'] = true;
    $data['message'] = 'Marca modificada con éxito!';
}

echo json_encode($data);
